Available channels for Norway:
==============

Can be found [here](http://xmltv.xmltv.se/channels-Norway.xml.gz). Too long to list here.

Notes:
-----------------

 * Some missing local channels which is hard to get in contact with.
