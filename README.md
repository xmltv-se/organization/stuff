XMLTV.se Project
==============

If you have any channel request then [create a new issue](https://gitlab.com/xmltv-se/organization/stuff/issues) if the channel isn't listed yet.
Be sure to check our channel list first. 

Please read information about your country in the "channels" folder. It contains information on why some information are missing.

Schedule updates are run every day at 22:00 GMT+1 (Stockholm) and most of the time finish at 04:30.

Issues
-----------------

Report any issues in the [issue tracker](https://gitlab.com/xmltv-se/organization/stuff/issues) and i'll try to fix it asap.

Rules
-----------------

 * Don't use the files for commercial products or projects. Use [Honeybee.it](http://honeybee.it/vip) for this.
 * Use proper User-Agents (we block blank ones).
 * Use your home connection (we block most server provider's ips)
 * Only import when needed. We provide [a file](http://xmltv.xmltv.se/datalist.xml.gz) which have Last-Modified timestamps for this.

How to use
-----------------

Please see the guides folder for how to use it in different OS:es etc.

Links
-----------------

 * [xmltv.xmltv.se](http://xmltv.xmltv.se) for usage in tv_grab tools
 * [json.xmltv.se](http://json.xmltv.se) for usage elsewhere

Help us
-----------------

If you want to help you can do pull requests with guides, info, etc.
You can also add better/updated logos to [channel-logos](https://gitlab.com/xmltv-se/channel-logos).

Backend
-----------------

 * [nonametv](https://github.com/xmltv-se/nonametv) is used for importing schedules and running it towards different APIs.
 * [channel-logos](https://gitlab.com/xmltv-se/channel-logos) is the channel logos.