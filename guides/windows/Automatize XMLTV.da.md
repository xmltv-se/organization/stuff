Automatize XMLTV (in Danish)
==============

Thanks 250266, from [recordere.dk](http://forum.recordere.dk/forum_posts.asp?TID=135355&PID=1452936&title=alternativer-til-timefortv#1452936):

>Her er lidt indledende forklaring. Jeg bruger MediaPortal med ArgusTv. Jeg henter guiden via programmet xmltv.exe (kan hentes fra [Sourceforge](http://sourceforge.net/projects/xmltv/files/xmltv/0.5.67/)).  Den tilhørende konfigurationsfil er sat op så jeg kun henter de TV programmer jeg har brug for. Guide data kommer fra xmltv.se. Sammen med xmltv.exe kommer en cmd fil (Grab_xmltv.cmd), som jeg har rettet lidt i. Linien som fjerner inkompatible tags vedr. MediaPortal har jeg udkommenteret, da det ikke giver bøvl i ArgusTV.
> Derefter har jeg lagt powershell scriptet ind, som nu kan modificere den hentede xml fil.
> Tilsidst flyttes xmlfilen over i ArgusTVs xmltv folder, som nu kan spise den.

Grab_xmltv.cmd:

```batchfile
@echo off

::==============================================::

::  Grab from xmltv.se and fix for MediaPortal  ::

::==============================================::



SET xmltvdata=D:\installationer\xmltv\Data_fra_xmltv

 CD "%xmltvdata%"

 ::Create config file

IF NOT EXIST "%xmltvdata%\tv_grab_se_tvzon.conf" ("%~dp0xmltv.exe" tv_grab_se_tvzon --config-file "%xmltvdata%\tv_grab_se_tvzon.conf" --configure)

 ::Downloads the file with xmltv

"%~dp0xmltv.exe" tv_grab_se_tvzon --config-file "%xmltvdata%\tv_grab_se_tvzon.conf" --days 14 --output "%xmltvdata%\tvguide_temp.xml"



::Remove lines incompatible with MediaPortal

::"%~dp0SED.EXE" -i "/<episode-num system=\"thetvdb.com\">/d" "%xmltvdata%\tvguide_temp.xml"



powershell -file "D:\installationer\xmltv\modificer tv guide.ps1



::Replace old file

MOVE /Y "%xmltvdata%\tvguide_temp.xml" "C:\ProgramData\ARGUS TV\XMLTV\tvguide.xml"

 CD "%~dp0"

 :END
```

> Forklaring til powershell scriptet.
> For at arbejde med ting i powershell er det godt at starte den version, som hedder Windows PowerShell ISE. Der kommer nemlig en brugbar editor med. Den er rigtig god til udvikling, da man kan markere sine linjer, og derefter afvikle dem med F8.

>Tryk på windows tasten, og skriv blot powershell, så kommer mulighederne frem. Start den gerne første gang med ”Run as administrator”, og kør denne kommando, som giver mulighed for at afvikle powershell scrip fra en alm. cmd promt.

>Kør denne i powershell editoren:

```batchfile
Set-ExecutionPolicy remotesigned
```

>Selve powershell scriptet kommer her. Den starter med at loade xml filen, der skal ændres. Til sidst overskrives den med ændringerne. En god idé er at rette filnavn i bunden så der dannes en ny fil i stedet for at overskrive. Så er det nemmere at teste igen og igen.

```xml
$XmlDocument = Get-Content -Path D:\installationer\xmltv\Data_fra_xmltv\tvguide_temp.xml -Encoding UTF8

 $xmldocument.GetType().FullName

   foreach($sub in $XmlDocument.tv.programme)

 {

    # Find om der evt. er kategorier, og gem den sidste i en variabel

   $BygSubTitle = ''

   if ($sub.category.count -ne 0)

   {

     $BygSubTitle = $sub.category[$sub.category.count-1].'#text'

    }



   # Find om der evt er dato

   if ($sub.date.count -ne 0)

   {

     if ($BygSubTitle -eq '')

     {

       $BygSubTitle = 'Fra ' + $sub.date

     }

     else

     {

       $BygSubTitle = $BygSubTitle + ' fra ' + $sub.date

     }

   }

   # Find om der evt. er star rating

   if ($sub.'star-rating'.count -ne 0)

   {

     if ($BygSubTitle -eq '')

     {

       $Bygsubtitle = 'Star ' + $Sub.'star-rating'.value

     }

     else

     {

     $BygSubTitle = $BygSubTitle + ' star ' + $sub.'star-rating'.value

     }

   }



  if ($sub.'sub-title'.'#text' -ne $null)

  {

    $A = $sub.'sub-title'.'#text'

    $B = $BygSubTitle + ' - ' + $A

    $B

    $sub.'sub-title'.'#text' = $B

  }

  else

  {

     $new = $xmldocument.CreateElement("sub-title")

    $NewText = $XmlDocument.CreateTextNode($BygSubTitle)

    $new.AppendChild($NewText)



    $Att = $XmlDocument.CreateAttribute("lang")

    $Att.value = "da"

    $new.Attributes.Append($Att)



    $sub.InsertBefore($new,$sub.desc)

  }



}

$NewPath ="D:\installationer\xmltv\Data_fra_xmltv\tvguide_temp.xml"

$Xmldocument.Save($NewPath)
```
