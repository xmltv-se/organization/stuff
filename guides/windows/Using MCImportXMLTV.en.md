![MCimportXMLTV](http://i.imgur.com/4mCuk7a.png)

Info about MCimportXMLTV (in English)
==============

This program was created to get the most out of the XMLTV feed via TIMEFOR.TV, but it has also been tested using other feed than TIMEFOR.TV, but I do not know of other suppliers who can provide these types of data in Denmark.

**To use the new program with xmltv.se:**

1. If you have used timefor.tv before and MCImportXMLTV v1.5a, then download v1.6a and install on top of old version
2. In Windows Media Center goto Settings –> Tv –> TV Guide –> Edit Channels –> Restore to default (Gendan standards)
3. In the MCImportXMLTV GUI Choose below “Tools” tab to delete all and start from scratch
4. Use the “Preset” button to select the country EPG needed
5. Press the “Fetch Data” to load the list of channels
6. Configure channels you want to import into media center, and assign it to MCE channels if you do not want to do that in Media Center directly
7. Press the “Import from saved XMLTV file” button and your new data is inside Windows Media Center
8. If you want the program to run every night got the tools tab and create a task at the wanted time
9. If you did not let my tool assign the channels then do it in Media Center now

**Download from [mcefun.nrossen.dk](http://mcefun.nrossen.dk/page/Downloads.aspx).**

![MCimportXMLTV](http://i.imgur.com/8btv8id.png)


Images and text from Niels.

Donate to Niels
-----------------

Click [here to donate](http://mcefun.nrossen.dk/page/Frivillige-donationer.aspx) to Niels (creator of MCimportXMLTV).
