Using Beachman's Script (in Swedish)
==============

Från [minhembio.com](http://www.minhembio.com/forum/index.php?showtopic=298445&page=1):

> Hej på er!.
>
> Tröttnade på WMC EPG, så jag gick över till xmltv via dvblink. Jag hittade dock ingen xmltv-tjänst som kunde leverara HD-flaggor och programikoner/posters som WMC supportar... Prövade ett tag WebGrab+, men tyckte den strulade allt för mycket.
>
> Jag har nu snabbt skrivit en grabber som primärt går mot http://xmltv.xmltv.se/ och dessutom kompletterar med posters från http://www.themoviedb.org/ ,http://thetvdb.com/ och http://tv.nu. Grabbern översätter också "star rating" till 5 stjärnor för att vara kompatibelt med WMC.
>
> I inställningsfilen (Tvzon.exe.config) går det att ändra vilka kanaler som den ska "Grabba". Gå in på http://xmltv.xmltv.se/channels.xml.gz och skriv in id för de kanaler som ska "Grabbas". Inställningsfilen har även en mapper för http://tv.nu/ som den använder sig av om den inte hittar posters på http://www.themoviedb.org/ eller http://thetvdb.com/. Där skriver man in vilka tvzon id som ska mappas till id på http://tv.nu/ . För att ta reda på id på tv.nu så väljer du bara den kanal som du är ute efter exempelvis http://www.tv.nu/kanal/svt1hd, id för svt1 hd på tv.nu är då "svt1hd".
>
>
> Grabbern är förkonfigurerad för 7 dagar epg boxer max med hd mot dvblinks standardsökväg för xmltv:
> C:\ProgramData\DVBLogic\DVBLink\xmltv\tvzon.xml. Dessa inställningar går också att ändra på i filen Tvzon.exe.config.
>
> Kräver .net framework 4
>
> Programmet bygger upp en cache för posters, så det kommer att rulla på snabbare andra gången grabbern körs. I dagsläget skriver den till fil först när den är klar, ni kan ändra antalet dagar i config
1=nuvarande dag.
>
> Pröva gärna!

Senaste versionen som funkar mot xmltv.se finns på [winows-tvzon repot](https://github.com/xmltv-se/windows-tvzon).

Adresserna finns numera i config-filen:
```xml
<setting name="ChannelUrl" serializeAs="String">
 <value>http://xmltv.xmltv.se/{0}_{1:yyyy-MM-dd}.xml.gz</value>
</setting>
<setting name="ChannelsUrl" serializeAs="String">
 <value>http://xmltv.xmltv.se/channels-Sweden.xml.gz</value>
</setting>
<setting name="ChannelsLastModifiedUrl" serializeAs="String">
 <value>http://xmltv.xmltv.se/datalist.xml.gz</value>
</setting>
```

Ny funktion är att det nu går att konfigurera sändningstider för en viss kanal. Exempelvis CMore på boxer:
```xml
<setting name="ChannelSettings" serializeAs="String">
    <value>
      {
      channels: [
        {
          channelId: "hitshd.cmore.se",
          monday: { start: "07:00", end:"16:30"},
          tuesday: { start: "00:30", end:"16:30"},
          wednesday: { start: "00:30", end:"16:30"},
          thursday: { start: "00:30", end:"16:30"},
          friday: { start: "00:30", end:"23:59"},
          saturday: { start: "00:00", end:"07:00"},
          sunday: { start: "00:00", end:"00:00"}
        },
        {
          channelId: "emotion.cmore.se",
          monday: { start: "01:00", end:"18:30"},
          tuesday: { start: "01:00", end:"18:30"},
          wednesday: { start: "01:00", end:"18:30"},
          thursday: { start: "01:00", end:"18:30"},
          friday: { start: "01:00", end:"18:30"},
          saturday: { start: "00:00", end:"00:00"},
          sunday: { start: "00:00", end:"00:00"}
        },
        {
          channelId: "sf-kanalen.cmore.se",
          monday: { start: "01:00", end:"18:00"},
          tuesday: { start: "01:00", end:"18:00"},
          wednesday: { start: "01:00", end:"18:00"},
          thursday: { start: "01:00", end:"18:00"},
          friday: { start: "01:00", end:"18:00"},
          saturday: { start: "01:00", end:"12:00"},
          sunday: { start: "01:00", end:"12:00"}
        }
      ]
      }
  	</value>
</setting>
```
