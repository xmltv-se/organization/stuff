Linux kvikguide til XMLTV.se (in Danish)
==============

From [recordere.dk](http://forum.recordere.dk/forum_posts.asp?TID=135355&PID=1433643&title=alternativer-til-timefortv#1433643), thanks Cyberguyen.

1. Hent XMLTV perl filerne fra http://sourceforge.net/projects/xmltv/files/xmltv/

2. Installer Perl (det plejer at være noget **sudo apt-get install perl**)

3. Hent ekstra moduler til at kompilere perl med:
  * cpan install Date::Manip
  * cpan install XML::DOM
  * cpan install XML::Twig
  * cpan install Tk
  * cpan install Tk::TableMatrix
  * cpan install HTTP::Cache::Transparent
  * cpan install DateTime
  * cpan install DateTime::Format::ISO8601
  * cpan install XML::Writer
  * cpan install XML::TreePP
  * cpan install Lingua::EN::Numbers::Ordinate
  * cpan install Lingua::Preferred
  * cpan install Term::ProgressBar
  * cpan install Unicode::String
  * cpan install Unicode::UTF8simple
  * cpan install Switch
  * cpan install HTML::FormatText

4. Gå ind i xmltv mappen og kør **dmake clean**

5.  Kør **perl Makefile.PL** fra xmltv mappen og bestem hvilke grabbers som ønskes. Bare vælg alle selvom du kun behøver **tv_grab_se_tvzon** (den er case sensitive)

6. Kør **dmake install** for at installere Perl scriptet i binaries

7. Skift over til en anden mappe.

8. Kør: **tv_grab_se_tvzon --configure** benyt: http://xmltv.xmltv.se/channels-Denmark.xml.gz **(or replace Denmark with your country)**

9. Angiv placering af Cache folder

10. Vælg de kanaler som ønskes (or type **all** for all)

11. Kør: **tv_grab_se_tvzon --days 14 --output tvguide.xml**
